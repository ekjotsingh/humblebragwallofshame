# **HumblebragWallOfShame :-** #

1) Using this application, a user can log in to twitter, compose a tweet (text and image) and post it on their behalf. Logout option is also provided in ActionBar.

2) On user's timeline, it fetches 100 most recent users who have been singled out on twitter handle "@HumbleBrag" and shows their profile image, username, screen name, and their tweet.

3) On tapping the tweet, it is shown in full screen. The user can swipe left and right to move to the next or previous tweet user in the list.

**Todo** : Post tweet with image attachment(Requires Native Twitter App to post image).
**Todo** : RecyclerView to show tweet list(instead ListView is used).

![Login Activity.png](https://bitbucket.org/repo/p44Kz7j/images/3630760662-Login%20Activity.png)
![Login Screen.png](https://bitbucket.org/repo/p44Kz7j/images/2821801590-Login%20Screen.png)![Authorize app.png](https://bitbucket.org/repo/p44Kz7j/images/1154437551-Authorize%20app.png)![Connect screen(if Native twitter app is installed).png](https://bitbucket.org/repo/p44Kz7j/images/1336944726-Connect%20screen(if%20Native%20twitter%20app%20is%20installed).png)![Activity to show tweets from @Humblebrag.png](https://bitbucket.org/repo/p44Kz7j/images/1039859646-Activity%20to%20show%20tweets%20from%20@Humblebrag.png)![Activity to show User info.png](https://bitbucket.org/repo/p44Kz7j/images/2348575021-Activity%20to%20show%20User%20info.png)![Composing and Posting Tweet.png](https://bitbucket.org/repo/p44Kz7j/images/348223581-Composing%20and%20Posting%20Tweet.png)![Logout Dialog.png](https://bitbucket.org/repo/p44Kz7j/images/1330148586-Logout%20Dialog.png)